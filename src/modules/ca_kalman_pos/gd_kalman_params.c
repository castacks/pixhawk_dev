/****************************************************************************
 *
 *   Copyright (C) 2013 Anton Babushkin. All rights reserved.
 *   Author: 	Geetesh Dubey	<geeteshdubey@gmail.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name PX4 nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/*
 * @file position_estimator_inav_params.c
 *
 * Parameters for position_estimator_inav
 */

#include <systemlib/param/param.h>

PARAM_DEFINE_FLOAT(CK_W_Z_BARO, 0.5f);
PARAM_DEFINE_FLOAT(CK_W_Z_GPS_P, 0.005f);
PARAM_DEFINE_FLOAT(CK_W_Z_ACC, 20.0f);
PARAM_DEFINE_FLOAT(CK_W_Z_SONAR, 3.0f);
PARAM_DEFINE_FLOAT(CK_W_XY_GPS_P, 1.0f);
PARAM_DEFINE_FLOAT(CK_W_XY_GPS_V, 2.0f);
PARAM_DEFINE_FLOAT(CK_W_XY_ACC, 20.0f);
PARAM_DEFINE_FLOAT(CK_W_XY_FLOW, 5.0f);
PARAM_DEFINE_FLOAT(CK_W_XY_VO, 1.0f);
PARAM_DEFINE_FLOAT(CK_W_XY_G, 10.0f);
PARAM_DEFINE_FLOAT(CK_W_GPS_FLOW, 0.1f);
PARAM_DEFINE_FLOAT(CK_W_ACC_BIAS, 0.05f);
PARAM_DEFINE_FLOAT(CK_FLOW_K, 0.15f);
PARAM_DEFINE_FLOAT(CK_FLOW_Q_MIN, 0.5f);
PARAM_DEFINE_FLOAT(CK_SONAR_FILT, 0.05f);
PARAM_DEFINE_FLOAT(CK_SONAR_ERR, 0.5f);
PARAM_DEFINE_FLOAT(CK_LAND_T, 3.0f);
PARAM_DEFINE_FLOAT(CK_LAND_DISP, 0.7f);
PARAM_DEFINE_FLOAT(CK_LAND_THR, 0.3f);
PARAM_DEFINE_INT32(CK_USE_FLOW, 1);
PARAM_DEFINE_INT32(CK_USE_VO, 1);
PARAM_DEFINE_INT32(CK_USE_G, 1);
PARAM_DEFINE_INT32(CK_USE_ACC, 1);
PARAM_DEFINE_FLOAT(CK_CORR_VTH, 0.05);
PARAM_DEFINE_FLOAT(CK_SENS_TF, 0.18);
PARAM_DEFINE_FLOAT(CK_Q_VO_G, 0.002);
PARAM_DEFINE_FLOAT(CK_Q_VO_B, 1.0);
PARAM_DEFINE_FLOAT(CK_Q_OF_G, 0.001);
PARAM_DEFINE_FLOAT(CK_Q_OF_B, 1.0);
PARAM_DEFINE_FLOAT(CK_Q_SO_G, 0.01);
PARAM_DEFINE_FLOAT(CK_Q_SO_B, 1.0);
PARAM_DEFINE_FLOAT(CK_Q_LA_G, 0.01);
PARAM_DEFINE_FLOAT(CK_Q_LA_B, 1.0);
PARAM_DEFINE_FLOAT(CK_Q_ACC_G, 0.01);
PARAM_DEFINE_FLOAT(CK_Q_ACC_B, 1.0);
PARAM_DEFINE_FLOAT(CK_Q_VOZ_G, 0.1);
PARAM_DEFINE_FLOAT(CK_Q_VOZ_B, 1.0);
PARAM_DEFINE_FLOAT(CK_Q_G_G, 0.8);
PARAM_DEFINE_FLOAT(CK_Q_G_B, 10.0);
PARAM_DEFINE_FLOAT(CK_Q_R_XY, 10.0);
PARAM_DEFINE_FLOAT(CK_Q_R_Z, 10.0);
