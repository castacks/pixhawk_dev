/**
 * @file gd_kalman.cpp
 * kalman position estimator.
 *
 * @author Geetesh Dubey
 */

#include <poll.h>

#include "gd_kalman.hpp"
#include <systemlib/err.h>

#include <systemlib/param/param.h>


//// constants
//// Titterton pg. 52
//static const float omega = 7.2921150e-5f; // earth rotation rate, rad/s
//static const float R0 = 6378137.0f; // earth radius, m
//static const float g0 = 9.806f; // standard gravitational accel. m/s^2
//static const int8_t ret_ok = 0; 		// no error in function
//static const int8_t ret_error = -1; 	// error occurred

//	memset(&ref, 0, sizeof(ref));
//	F.zero();
    // initial state covariance matrix
//	P0.identity();
//	P0 *= 0.01f;


//math::Quaternion gd_kalman::init(float ax, float ay, float az, float mx, float my, float mz)

//		Vector<3> w(_sensors.gyro_rad_s);
//q.normalize();
//    C_nb = q.to_dcm();
        // attitude
//		q = q + q.derivative(w) * dt;

        // renormalize quaternion if needed
//		if (fabsf(q.length() - 1.0f) > 1e-4f) {

//		Vector<3> euler = C_nb.to_euler();
//		Vector<3> accelB(_sensors.accelerometer_m_s2);
//	P = P + (F * P + P * F.transposed() + G * V * G.transposed()) * dt;
//	float beta = y * (S.inversed() * y);


gd_kalman::gd_kalman()
{
    using namespace math;
    A.identity();
    C.zero();
//    float dt = 0.02;

    u.zero();
    z.zero();
    sig.zero();//identity();
//    sig = sig * 9999;

    Q.identity();
    R.zero();//identity();
//    R = R * 0.0009;//this is good
    R(6,6) =R(7,7) = 100;
    R(8,8) = 10;
//    R(3,3) =R(4,4) =R(5,5) = 10;
//    R(2,2) = 10;

//    R(2,2) = R(5,5) = 10.0;
//    for(int i = 0 ; i<9 ; i++)
//    {
//        R(i,i) = 10;
//        Q(i,i) = 1;
//    }

    Q(0,0) = Q(1,1) = Q(2,2) = V_B;
    Q(3,3) = Q(4,4) = Q(5,5) = F_B;
    Q(7,7) = Q(8,8) = Q(9,9) = A_B;
    Q(10,10) = Q(11,11) = Q(12,12) = G_B;

    new_sum = prev_sum = 0.0;
    avg_window_size = 5;
    meas_vec.clear();
    delta_yaw = 0.0;
}


void gd_kalman::predict(float dt)
{
//    warnx("b4 pred:");u.print();
    A(0,3) = A(1,4) =A(2,5) =A(3,6) =A(4,7) =A(5,8)= dt;
    A(0,6) = A(1,7) = A(2,9) = dt*dt;

    math::Vector<9> u_t = A * u;
    math::Matrix<9,9> sig_t = A * sig * A.transposed() + R;

    bool found_nan = false;
    for(int i=0;i<9;i++)
        if(!isfinite(u(i)))
        {
            found_nan = true;
            warnx("NAN in pred");
        }

    if(!found_nan)
    {
        u = u_t;
        sig = sig_t;

//        if(print_ctr == 100 )
//        {
//            warnx("S: \n:");
//            sig.print();
//        }
    }
}

void gd_kalman::correct(float dt)
{
//    warnx("Inside Correct");
    // If Optical Flow in Z is available.
//    C(0,3) = C(1,4) = C(2,5) = C(3,3) = C(4,4) = C(5,5) = C(6,2) = C(7,6) = C(8,7) = C(9,8) = 1.0;
//    C(0,6) = C(1,7) = C(2,8) = C(3,6) = C(4,7) = C(5,8) = C(6,5) = dt;
//    C(6,8) = dt*dt;

    //USE SONAR Z Measurement in INPUT
    C(0,3) = C(1,4) = C(2,5) = C(3,3) = C(4,4) = C(5,2) = C(6,2) = C(7,6) = C(8,7) = C(9,8) = C(10,0) = C(11,1) = 1.0;/*C(12,2) is Z GLOBAL*/
    C(0,6) = C(1,7) = C(2,8) = C(3,6) = C(4,7) = C(5,5) = C(6,5) = dt;
    C(5,8) = C(6,8) = dt*dt;


    math::Matrix<13,13> temp_inv = C * sig * C.transposed() + Q;
    math::Matrix<13,13> inv = temp_inv.inversed();
    math::Matrix<9,13> K = sig * C.transposed() * inv;

//    if(print_ctr == 100 )
//    {
//        warnx("K: \n");
//        K.print();
//    }

    //    math::Vector<9> U = u + K * (z - C * u);
    math::Vector<13> Cu = C * u;
    math::Vector<13> z_Cu = z - Cu;
    math::Vector<9> KzCu = K * z_Cu;
    math::Vector<9> U = u + KzCu;
//    warnx("U MAT: Good");
    math::Matrix<9,9> I;
    I.identity();

//    math::Matrix<9,9> Sig = (I - K * C) * sig;
    math::Matrix<9,9> KC = K*C;
    math::Matrix<9,9> I_KC = I - KC;
//    warnx("sleep 1");
//    sleep(1);
    math::Matrix<9,9> Sig = I_KC * sig;

    bool found_nan = false;
    for(int i=0;i<9;i++)
        if(!isfinite(U(i)))
        {
            found_nan = true;
            warnx("NAN in correct");
        }

    if(!found_nan)
    {
        u = U;
        sig = Sig;
    }
    //RESETTING STUFF
//    z.zero();
//    Q.identity();
}

int gd_kalman::parameters_init()
{
    param_handles_.w_z_baro = param_find("CK_W_Z_BARO");
    param_handles_.w_z_gps_p = param_find("CK_W_Z_GPS_P");
    param_handles_.w_z_acc = param_find("CK_W_Z_ACC");
    param_handles_.w_z_sonar = param_find("CK_W_Z_SONAR");
    param_handles_.w_xy_gps_p = param_find("CK_W_XY_GPS_P");
    param_handles_.w_xy_gps_v = param_find("CK_W_XY_GPS_V");
    param_handles_.w_xy_acc = param_find("CK_W_XY_ACC");
    param_handles_.w_xy_flow = param_find("CK_W_XY_FLOW");
    param_handles_.w_xy_vo = param_find("CK_W_XY_VO");
    param_handles_.w_xy_g = param_find("CK_W_XY_G");
    param_handles_.w_gps_flow = param_find("CK_W_GPS_FLOW");
    param_handles_.w_acc_bias = param_find("CK_W_ACC_BIAS");
    param_handles_.flow_k = param_find("CK_FLOW_K");
    param_handles_.flow_q_min = param_find("CK_FLOW_Q_MIN");
    param_handles_.sonar_filt = param_find("CK_SONAR_FILT");
    param_handles_.sonar_err = param_find("CK_SONAR_ERR");
    param_handles_.land_t = param_find("CK_LAND_T");
    param_handles_.land_disp = param_find("CK_LAND_DISP");
    param_handles_.land_thr = param_find("CK_LAND_THR");
    param_handles_.use_flow = param_find("CK_USE_FLOW");
    param_handles_.use_vo = param_find("CK_USE_VO");
    param_handles_.use_g = param_find("CK_USE_G");
    param_handles_.use_acc = param_find("CK_USE_ACC");
    param_handles_.corr_v_th = param_find("CK_CORR_VTH");
    param_handles_.sens_tf = param_find("CK_SENS_TF");
    param_handles_.q_vo_good = param_find("CK_Q_VO_G");
    param_handles_.q_vo_bad = param_find("CK_Q_VO_B");
    param_handles_.q_flow_good = param_find("CK_Q_OF_G");
    param_handles_.q_flow_bad = param_find("CK_Q_OF_B");
    param_handles_.q_sonar_good = param_find("CK_Q_SO_G");
    param_handles_.q_sonar_bad = param_find("CK_Q_SO_B");
    param_handles_.q_laser_good = param_find("CK_Q_LA_G");
    param_handles_.q_laser_bad = param_find("CK_Q_LA_B");
    param_handles_.q_acc_good = param_find("CK_Q_ACC_G");
    param_handles_.q_acc_bad = param_find("CK_Q_ACC_B");
    param_handles_.q_vo_z_good = param_find("CK_Q_VOZ_G");
    param_handles_.q_vo_z_bad = param_find("CK_Q_VOZ_B");    
    param_handles_.q_g_good = param_find("CK_Q_G_G");
    param_handles_.q_g_bad = param_find("CK_Q_G_B");

    param_handles_.q_R_XY = param_find("CK_Q_R_XY");
    param_handles_.q_R_Z = param_find("CK_Q_R_Z");

    return OK;
}

int gd_kalman::parameters_update()
{
    param_get(param_handles_.w_z_baro, &(params.w_z_baro));
    param_get(param_handles_.w_z_gps_p, &(params.w_z_gps_p));
    param_get(param_handles_.w_z_acc, &(params.w_z_acc));
    param_get(param_handles_.w_z_sonar, &(params.w_z_sonar));
    param_get(param_handles_.w_xy_gps_p, &(params.w_xy_gps_p));
    param_get(param_handles_.w_xy_gps_v, &(params.w_xy_gps_v));
    param_get(param_handles_.w_xy_acc, &(params.w_xy_acc));
    param_get(param_handles_.w_xy_flow, &(params.w_xy_flow));
    param_get(param_handles_.w_xy_vo, &(params.w_xy_vo));
    param_get(param_handles_.w_xy_g, &(params.w_xy_g));
    param_get(param_handles_.w_gps_flow, &(params.w_gps_flow));
    param_get(param_handles_.w_acc_bias, &(params.w_acc_bias));
    param_get(param_handles_.flow_k, &(params.flow_k));
    param_get(param_handles_.flow_q_min, &(params.flow_q_min));
    param_get(param_handles_.sonar_filt, &(params.sonar_filt));
    param_get(param_handles_.sonar_err, &(params.sonar_err));
    param_get(param_handles_.land_t, &(params.land_t));
    param_get(param_handles_.land_disp, &(params.land_disp));
    param_get(param_handles_.land_thr, &(params.land_thr));
    param_get(param_handles_.use_flow, &(params.use_flow));
    param_get(param_handles_.use_vo, &(params.use_vo));
    param_get(param_handles_.use_g, &(params.use_g));
    param_get(param_handles_.use_acc, &(params.use_acc));
    param_get(param_handles_.corr_v_th, &(params.corr_v_th));
    param_get(param_handles_.sens_tf, &(params.sens_tf));


    param_get(param_handles_.q_vo_good, &(params.q_vo_good));
    param_get(param_handles_.q_vo_bad, &(params.q_vo_bad));
    param_get(param_handles_.q_flow_good, &(params.q_flow_good));
    param_get(param_handles_.q_flow_bad, &(params.q_flow_bad));
    param_get(param_handles_.q_sonar_good, &(params.q_sonar_good));
    param_get(param_handles_.q_sonar_bad, &(params.q_sonar_bad));
    param_get(param_handles_.q_laser_good, &(params.q_laser_good));
    param_get(param_handles_.q_laser_bad, &(params.q_laser_bad));
    param_get(param_handles_.q_acc_good, &(params.q_acc_good));
    param_get(param_handles_.q_acc_bad, &(params.q_acc_bad));
    param_get(param_handles_.q_vo_z_good, &(params.q_vo_z_good));
    param_get(param_handles_.q_vo_z_bad, &(params.q_vo_z_bad));

    param_get(param_handles_.q_g_good, &(params.q_g_good));
    param_get(param_handles_.q_g_bad, &(params.q_g_bad));

    param_get(param_handles_.q_R_XY, &(params.q_R_XY));
    param_get(param_handles_.q_R_Z, &(params.q_R_Z));
    return OK;
}

void gd_kalman::inertial_filter_predict(float dt, float x[3])
{
    if (isfinite(dt)) {
        x[0] += x[1] * dt + x[2] * dt * dt / 2.0f;
        x[1] += x[2] * dt;
    }
}

void gd_kalman::inertial_filter_correct(float e, float dt, float x[3], int i, float w)
{
    if (isfinite(e) && isfinite(w) && isfinite(dt)) {
        float ewdt = e * w * dt;
        x[i] += ewdt;

        if (i == 0) {
            x[1] += w * ewdt;
            x[2] += w * w * ewdt / float(3.0);

        } else if (i == 1) {
            x[2] += w * ewdt;
        }
    }
}

void gd_kalman::write_debug_log(const char *msg, float dt, float x_est[3], float y_est[3], float z_est[3], float corr_acc[3], float corr_gps[3][2], float w_xy_gps_p, float w_xy_gps_v)
{
//    FILE *f = fopen("/fs/microsd/inav.log", "a");

//    if (f) {
//        char *s = new char[256];///malloc(256);
//        unsigned n = snprintf(s, 256, "%llu %s\n\tdt=%.5f x_est=[%.5f %.5f %.5f] y_est=[%.5f %.5f %.5f] z_est=[%.5f %.5f %.5f]\n", hrt_absolute_time(), msg, dt, x_est[0], x_est[1], x_est[2], y_est[0], y_est[1], y_est[2], z_est[0], z_est[1], z_est[2]);
//        fwrite(s, 1, n, f);
//        n = snprintf(s, 256, "\tacc_corr=[%.5f %.5f %.5f] gps_pos_corr=[%.5f %.5f %.5f] gps_vel_corr=[%.5f %.5f %.5f] w_xy_gps_p=%.5f w_xy_gps_v=%.5f\n", corr_acc[0], corr_acc[1], corr_acc[2], corr_gps[0][0], corr_gps[1][0], corr_gps[2][0], corr_gps[0][1], corr_gps[1][1], corr_gps[2][1], w_xy_gps_p, w_xy_gps_v);
//        fwrite(s, 1, n, f);
//        free(s);
//    }

//    fsync(fileno(f));
//    fclose(f);
}

void gd_kalman::write_debug_log2(const char *msg, float dt, math::Vector<9> data)
{
//    FILE *f = fopen("/fs/microsd/inav.log", "a");

//    if (f) {
//        char *s = new char[256];///malloc(256);
////        unsigned n = snprintf(s, 256, "%llu %.5f %.5f %.5f %.5f %.5f %.5f %.5f\n",
////                              hrt_absolute_time(), dt, data(0), data(1), data(2), data(3), data(4), data(5));
//        unsigned n = snprintf(s, 256, "%.5f %.5f %.5f %.5f %.5f %.5f %.5f %.5f\n",
//                              data(0), data(1), data(2), data(3), data(4), data(5),data(6), data(7));

//        fwrite(s, 1, n, f);
////        n = snprintf(s, 256, "\tacc_corr=[%.5f %.5f %.5f] gps_pos_corr=[%.5f %.5f %.5f] gps_vel_corr=[%.5f %.5f %.5f] w_xy_gps_p=%.5f w_xy_gps_v=%.5f\n", corr_acc[0], corr_acc[1], corr_acc[2], corr_gps[0][0], corr_gps[1][0], corr_gps[2][0], corr_gps[0][1], corr_gps[1][1], corr_gps[2][1], w_xy_gps_p, w_xy_gps_v);
////        fwrite(s, 1, n, f);
//        free(s);
//    }

//    fsync(fileno(f));
    //    fclose(f);
}

bool gd_kalman::mov_avg(float meas, float *res)
{
    bool valid = false;
    if(meas_vec.size() < avg_window_size)
    {
        meas_vec.push_back(meas);
        new_sum += meas;
        prev_sum = new_sum;
        *res = prev_sum/meas_vec.size();
//        warnx("FILL %.2f %10d",meas,int(meas_vec.size()));
    }
    else
    {//UPDATE MOVING AVG
        new_sum = prev_sum - meas_vec.front() + meas;
        meas_vec.pop_front();
        meas_vec.push_back(meas);
        if(fabs(meas-prev_sum/avg_window_size) < double(params.sonar_err)) //no spike
        {
            *res = meas;
            valid = true;
        }
        else
        {
            *res = (prev_sum/avg_window_size);
//            warnx("SPIKE %.2f %d",meas,int(meas_vec.size()));
        }
        prev_sum = new_sum;
    }
    return valid;
}

void gd_kalman::updateRot(struct vehicle_attitude_s *att)
{
    if((params.use_g == 1) && global_pose_valid){
        float r,p,y;
        r = p = y = 0.0;

        r = att->roll;
        p = att->pitch;
        y = att->yaw - delta_yaw;
        rotMat.from_euler(r,p,y);

        for(size_t i =0;i<3;i++)
            for(size_t j=0;j<3;j++)
                PX4_R(att->R, i, j)=float(rotMat(i,j));
    }
}

void gd_kalman::main()
{
    mavlink_fd = open(MAVLINK_LOG_DEVICE, 0);
//    mavlink_log_info(mavlink_fd, "[kalman_pose] started");

        float x_est[3] = { 0.0f, 0.0f, 0.0f };
    float y_est[3] = { 0.0f, 0.0f, 0.0f };
    float z_est[3] = { 0.0f, 0.0f, 0.0f };

    int acc_init_cnt = 0;
    int acc_init_num = 200;
    float acc_offset = 0.0f;		// baro offset for reference altitude, initialized on start, then adjusted
    float surface_offset = 0.0f;	// ground level offset from reference altitude
    float surface_offset_rate = 0.0f;	// surface offset change rate
    float alt_avg = 0.0f;
    bool landed = true;
    bool use_vo = false;
    bool use_global_pose = false;
    bool first_global = true;
//    float vo_pos_ned[3] = { 0.0f, 0.0f , 0.0f };
    hrt_abstime landed_time = 0;

    hrt_abstime accel_timestamp = 0;
    hrt_abstime acc_timestamp = 0;

//    bool ref_inited = false;
//    hrt_abstime ref_init_start = 0;
//    const hrt_abstime ref_init_delay = 1000000;	// wait for 1s after 3D fix
    struct map_projection_reference_s ref;
    memset(&ref, 0, sizeof(ref));

    uint16_t accel_updates = 0;
    uint16_t baro_updates = 0;
    uint16_t gps_updates = 0;
    uint16_t attitude_updates = 0;
    uint16_t flow_updates = 0;

    hrt_abstime updates_counter_start = hrt_absolute_time();
    hrt_abstime pub_last = hrt_absolute_time();

    hrt_abstime t_prev = 0;
    hrt_abstime t_prev_vo = 0;
    hrt_abstime t_prev_global_pose = 0;


    /* acceleration in NED frame */
    float accel_NED[3] = { 0.0f, 0.0f, -CONSTANTS_ONE_G };

    /* store error when sensor updates, but correct on each time step to avoid jumps in estimated value */
    float corr_acc[] = { 0.0f, 0.0f, 0.0f };	// N E D
    float acc_bias[] = { 0.0f, 0.0f, 0.0f };	// body frame
    float corr_baro = 0.0f;		// D
    float corr_gps[3][2] = {
        { 0.0f, 0.0f },		// N (pos, vel)
        { 0.0f, 0.0f },		// E (pos, vel)
        { 0.0f, 0.0f },		// D (pos, vel)
    };
    float w_gps_xy = 1.0f;
    float w_gps_z = 1.0f;
    float corr_sonar = 0.0f;
    float corr_sonar_filtered = 0.0f;

    float corr_flow[] = { 0.0f, 0.0f };	// N E
    float corr_vo[] = { 0.0f, 0.0f, 0.0f };	// N E
    float w_rel[] = { 1.0f, 1.0f };	// flow , vo
    float corr_global_update[] = { 0.0f, 0.0f };	// N E

    float debug_vel[] = { 0.0f, 0.0f, 0.0f, 0.0f };	// N E

    float w_flow = 0.0f;

    float sonar_prev = 0.0f;
    float laser_range = 0.0f;

    hrt_abstime flow_prev = 0;			// time of last flow measurement
    hrt_abstime sonar_time = 0;			// time of last sonar measurement (not filtered)
    hrt_abstime sonar_valid_time = 0;	// time of last sonar measurement used for correction (filtered)
    hrt_abstime laser_valid_time = 0;	// time of last laser altimeter measurement used for correction (filtered)
    hrt_abstime xy_src_time = 0;		// time of last available position data

//    bool gps_valid = false;			// GPS is valid
    bool sonar_valid = false;		// sonar is valid
    bool flow_valid = false;		// flow is valid
    bool flow_accurate = false;		// flow should be accurate (this flag not updated if flow_valid == false)

    /* declare and safely initialize all structs */
    struct actuator_controls_s actuator;
    memset(&actuator, 0, sizeof(actuator));
    struct actuator_armed_s armed;
    memset(&armed, 0, sizeof(armed));
    struct sensor_combined_s sensor;
    memset(&sensor, 0, sizeof(sensor));
    struct vehicle_gps_position_s gps;
    memset(&gps, 0, sizeof(gps));
    struct vehicle_attitude_s att;
    memset(&att, 0, sizeof(att));
    struct vehicle_local_position_s local_pos;
    memset(&local_pos, 0, sizeof(local_pos));
    struct optical_flow_s flow;
    memset(&flow, 0, sizeof(flow));
    struct vehicle_global_position_s global_pos;
    memset(&global_pos, 0, sizeof(global_pos));
    struct ca_fovis_state_s fovis_state;
    memset(&fovis_state, 0, sizeof(fovis_state));
    struct ca_global_state_struct_s global_state;
    memset(&global_state, 0, sizeof(global_state));
    struct state_estimator_debug_s state_debug;
    memset(&state_debug, 0, sizeof(state_debug));
    struct range_finder_report range_report;
    memset(&range_report, 0, sizeof(range_report));
    /* subscribe */
    int parameter_update_sub = orb_subscribe(ORB_ID(parameter_update));
    int actuator_sub = orb_subscribe(ORB_ID_VEHICLE_ATTITUDE_CONTROLS);
    int armed_sub = orb_subscribe(ORB_ID(actuator_armed));
    int sensor_combined_sub = orb_subscribe(ORB_ID(sensor_combined));
    int vehicle_attitude_sub = orb_subscribe(ORB_ID(vehicle_attitude));
    int optical_flow_sub = orb_subscribe(ORB_ID(optical_flow));
//    int vehicle_gps_position_sub = orb_subscribe(ORB_ID(vehicle_gps_position));
    int ca_fovis_state_sub = orb_subscribe(ORB_ID(ca_fovis_state));
    int ca_global_state_sub = orb_subscribe(ORB_ID(ca_global_state_msg));
    int laser_range_sub = orb_subscribe(ORB_ID(sensor_range_finder));

    /* advertise */
    orb_advert_t vehicle_local_position_pub = orb_advertise(ORB_ID(vehicle_local_position), &local_pos);
    orb_advert_t vehicle_global_position_pub = orb_advertise(ORB_ID(vehicle_global_position), &global_pos);
    orb_advert_t state_estimator_debug_pub = orb_advertise(ORB_ID(state_estimator_debug), &state_debug);

    /* initialize parameter handles */
    parameters_init();

    /* first parameters read at start up */
    struct parameter_update_s param_update;
    orb_copy(ORB_ID(parameter_update), parameter_update_sub, &param_update); /* read from param topic to clear updated flag */

    /* first parameters update */
    parameters_update();

//    struct pollfd fds_init[1] = {
//        { .fd = sensor_combined_sub, .events = POLLIN },
//    };

    struct pollfd fds_init[1];
    fds_init[0].fd = sensor_combined_sub;
    fds_init[0].events = POLLIN;

    /* wait for initial baro value */
    bool wait_acc = true;

    thread_running = true;

    while (wait_acc && !thread_should_exit) {
        int ret = poll(fds_init, 1, 1000);

        if (ret < 0) {
            /* poll error */
            mavlink_log_info(mavlink_fd, "[inav] poll error on init");

        } else if (ret > 0) {
            if (fds_init[0].revents & POLLIN) {
                orb_copy(ORB_ID(sensor_combined), sensor_combined_sub, &sensor);

//                if (wait_baro && sensor.baro_timestamp != baro_timestamp) {
//                    baro_timestamp = sensor.baro_timestamp;

//                    /* mean calculation over several measurements */
//                    if (baro_init_cnt < baro_init_num) {
//                        baro_offset += sensor.baro_alt_meter;
//                        baro_init_cnt++;

//                    } else {
//                        wait_baro = false;
//                        baro_offset /= (float) baro_init_cnt;
//                        warnx("baro offs: %.2f", baro_offset);
//                        mavlink_log_info(mavlink_fd, "[inav] baro offs: %.2f", baro_offset);
//                        local_pos.z_valid = true;
//                        local_pos.v_z_valid = true;
//                        global_pos.baro_valid = true;
//                    }
//                }
                if (wait_acc && sensor.accelerometer_timestamp != acc_timestamp) {
                    acc_timestamp = sensor.accelerometer_timestamp;

                    /* mean calculation over several measurements */
                    if (acc_init_cnt < acc_init_num) {
                        acc_offset += sensor.accelerometer_m_s2[2];
                        acc_init_cnt++;

                    } else {
                        wait_acc = false;
                        acc_offset /= (float) acc_init_cnt;
                        warnx("Acc offs: %.2f", acc_offset);
                        mavlink_log_info(mavlink_fd, "[inav] Acc offs: %.2f", acc_offset);
                        local_pos.z_valid = true;
                        local_pos.v_z_valid = true;
//                        global_pos.baro_valid = true;
                    }
                }
            }
        }
    }

    /* main loop */
//    struct pollfd fds[1] = {
//        { .fd = vehicle_attitude_sub, .events = POLLIN },
//    };
    struct pollfd fds[1];
    fds[0].fd = sensor_combined_sub;
    fds[0].events = POLLIN;



    while (!thread_should_exit) {

        usleep(5000);

        int ret = poll(fds, 1, 20); // wait maximal 20 ms = 50 Hz minimum rate
        hrt_abstime t = hrt_absolute_time();

        if (ret < 0) {
            /* poll error */
            mavlink_log_info(mavlink_fd, "[inav] poll error on init");
            continue;

        } else if (ret > 0) {
            /* act on attitude updates */

            /* vehicle attitude */
            orb_copy(ORB_ID(vehicle_attitude), vehicle_attitude_sub, &att);
            attitude_updates++;

            //Update attitude yaw deviation from Global pose (if global valid)
            updateRot(&att);

            bool updated;

            /* parameter update */
            if(param_update_ctr == 100)
            {
                param_update_ctr = 0;
                orb_check(parameter_update_sub, &updated);

                if (updated) {
                    struct parameter_update_s update;
                    orb_copy(ORB_ID(parameter_update), parameter_update_sub, &update);
                    parameters_update();
                    R(6,6) =R(7,7) = params.q_R_XY;
                    R(8,8) = params.q_R_Z;
                }
            }
            else
                param_update_ctr++;

            /* actuator */
            orb_check(actuator_sub, &updated);

            if (updated) {
                orb_copy(ORB_ID_VEHICLE_ATTITUDE_CONTROLS, actuator_sub, &actuator);
            }

            /* armed */
            orb_check(armed_sub, &updated);

            if (updated) {
                orb_copy(ORB_ID(actuator_armed), armed_sub, &armed);
            }

            /* sensor combined */
            orb_check(sensor_combined_sub, &updated);

            if (updated) {
                orb_copy(ORB_ID(sensor_combined), sensor_combined_sub, &sensor);
                bool acc_good = false;

                if (sensor.accelerometer_timestamp != accel_timestamp) {
                    if (att.R_valid) {
                        /* correct accel bias */
//                        acc_bias[0] = -0.16;
//                        acc_bias[0] = 0.38;
                        sensor.accelerometer_m_s2[0] -= acc_bias[0];
                        sensor.accelerometer_m_s2[1] -= acc_bias[1];
                        sensor.accelerometer_m_s2[2] -= acc_bias[2];

                        /* transform acceleration vector from body frame to NED frame */
                        for (int i = 0; i < 3; i++) {
                            accel_NED[i] = 0.0f;

                            for (int j = 0; j < 3; j++) {
                                accel_NED[i] += PX4_R(att.R, i, j) * sensor.accelerometer_m_s2[j];
                            }
                        }

                        corr_acc[0] = accel_NED[0] - x_est[2];
                        corr_acc[1] = accel_NED[1] - y_est[2];
                        corr_acc[2] = accel_NED[2] + CONSTANTS_ONE_G - z_est[2];

                        acc_good = true;
                    } else {
                        memset(corr_acc, 0, sizeof(corr_acc));
                    }

                    if((params.use_acc == 1) && acc_good)
                    {
                        z(7) = accel_NED[0];
                        z(8) = accel_NED[1];
                        z(9) =  accel_NED[2] + CONSTANTS_ONE_G;//- acc_offset;//CONSTANTS_ONE_G;//accel_NED[2];
                        Q(7,7) = Q(8,8) = Q(9,9) = params.q_acc_good;
                    }
                    else
                        Q(7,7) = Q(8,8) = Q(9,9) = params.q_acc_bad;


                    accel_timestamp = sensor.accelerometer_timestamp;
                    accel_updates++;
                }

//                if (sensor.baro_timestamp != acc_timestamp) {
//                    corr_baro = acc_offset - sensor.baro_alt_meter - z_est[0];
//                    acc_timestamp = sensor.baro_timestamp;
//                    baro_updates++;
//                }
            }

            /* global pose update */
            use_global_pose = false;
            orb_check(ca_global_state_sub,&updated);

            if(updated && (params.use_g == 1))
            {
                orb_copy(ORB_ID(ca_global_state_msg), ca_global_state_sub, &global_state);
                t_prev_global_pose = t;
                use_global_pose = true;

//                mavlink_log_info(mavlink_fd, "[inav] Recvd Global State: %.2f %.2f", global_state.pose[0],global_state.pose[1]);

                //COMPUTE REQUIRED CORRECTION
                corr_global_update[0]=global_state.pose[0] - x_est[0];//correction in position
                corr_global_update[1]=global_state.pose[1] - y_est[0];//correction in position

//                math::Vector<2> pose_g,pose_l;
//                pose_g(0) = global_state.pose[0];
//                pose_g(1) = global_state.pose[1];

//                if (isfinite(att.yaw))
//                {
//                    delta_yaw = (att.yaw - global_state.pose[6]);
//                    rotMat(0,0) = cos(delta_yaw);
//                    rotMat(0,1) = -sin(delta_yaw);
//                    rotMat(1,0) = sin(delta_yaw);
//                    rotMat(1,1) =  cos(delta_yaw);
//                    pose_l = rotMat * pose_g;

//                    global_state.pose[0] = pose_l(0);
//                    global_state.pose[1] = pose_l(1);
//                }
                delta_yaw = att.yaw - global_state.pose[6];
                z(10) = global_state.pose[0];
                z(11) = global_state.pose[1];
//                z(12) = global_state.pose[2];
                Q(10,10) = Q(11,11) = params.q_g_good;

                if(first_global)
                {
                    u(0) = global_state.pose[0];
                    u(1) = global_state.pose[1];
                    first_global = false;
                }
            }
            else
                Q(10,10) = Q(11,11) = params.q_g_bad;

            /* laser_range */
            laser_good = false;
            orb_check(laser_range_sub,&updated);

            if(updated)
            {
                orb_copy(ORB_ID(sensor_range_finder), laser_range_sub, &range_report);
                laser_good = false;

                if(range_report.valid)
                {
//                    mavlink_log_info(mavlink_fd, "[inav] Recvd Laser Range: %.2f %d", range_report.distance,range_report.valid);

//                    laser_range = 0.0;

//                    float range_vector[3] = { 0.0f, 0.0f, range_report.distance};

                    //APPLYING BODY ROTATION
//                        for (int j = 0; j < 3; j++) {
                            laser_range = PX4_R(att.R, 2,2) * range_report.distance;
//                        }

                    if (isfinite(laser_range) && laser_range > 0.31)
                    {
                        laser_valid_time = t;
                        laser_good = true;
                    }
                }
                if (laser_good)
                {
                    z(6) =  -laser_range;
                    Q(6,6) = params.q_laser_good;
                }
                else
                    Q(6,6) = params.q_laser_bad;
            }


            /* fovis */
            use_vo = false;
            orb_check(ca_fovis_state_sub,&updated);

            if(updated && (params.use_vo == 1))
            {
                orb_copy(ORB_ID(ca_fovis_state), ca_fovis_state_sub, &fovis_state);


                if(fovis_state.estimate_status == 1)
                {
//                    mavlink_log_info(mavlink_fd, "RPY: %.1f %.1f %.1f", att.rollspeed*180/M_PI, att.pitchspeed*180/M_PI, att.yaw*180/M_PI);

                    t_prev_vo = t;
                    use_vo = true;

                    float vo_vel[4] = { 0.0f, 0.0f, 0.0f, 1.0f };
                    float vo_vel_ned[3] = { 0.0f, 0.0f, 0.0f };

                    if(1)//SB_QUAD
                    {
                        vo_vel[1] = fovis_state.fovis_pose[0];// /(fovis_state.time_del_nsec/1e9);
                        vo_vel[0] = fovis_state.fovis_pose[2];// /(fovis_state.time_del_nsec/1e9);
                        vo_vel[2] = fovis_state.fovis_pose[1];//z_est[1];//TODO should be the val from VO

                        float vo_vel_pitched[3] = { 0.0f, 0.0f, 0.0f };
//                        if(0) //CORRECTING FOR STATIC PITCH ROTATION
//                        {
//                            float theta = 0.26f;//15deg
//                            //                      cosθ    0     sinθ
//                            //                          0   1     0
//                            //                      −sinθ   0    cosθ

//                            float R[4][4];//ROTATION+TRANSLATION MAT FOR FIXED PITCH OFFSET
//                            R[0][0] = cosf(theta);  R[0][1] = 0.0f; R[0][2] = sinf(theta);  //R[0][3] = 0.0f;
//                            R[1][0] = 0.0f;         R[1][1] = 1.0f; R[1][2] = 0.0f;         //R[1][3] = 0.0f;
//                            R[2][0] = -sinf(theta); R[2][1] = 0.0f; R[2][2] = cosf(theta);  //R[2][3] = 0.01f;//10cm
//                            //                        R[3][0] = 0.0f;         R[3][1] = 0.0f; R[3][2] = 0.0f;         R[3][3] = 1.0f;

//                            //INSTEAD OF TRANSLATION TRY SUBTRACTING wR i.e. V = Vest - wR

//                            for (int i = 0; i < 3; i++) {
//                                for (int j = 0; j < 3; j++) {
//                                    vo_vel_pitched[i] += R[i][j] * vo_vel[j];
//                                }
//                            }
//                        }
//                        else // MEANS SENSOR IS MOUNTED LOOKING STRAIGHT AHEAD
//                        {
                            vo_vel_pitched[0] = vo_vel[0];
                            vo_vel_pitched[1] = vo_vel[1];
                            vo_vel_pitched[2] = vo_vel[2];
//                        }

                        //CORRECTING FOR TRANSLATION
                        if (isfinite(att.pitchspeed))
                            vo_vel_pitched[0] -= att.pitchspeed * params.sens_tf;//23cm translation
                        if (isfinite(att.rollspeed))
                            vo_vel_pitched[1] += att.rollspeed * params.sens_tf;

                        //APPLYING BODY ROTATION
                        for (int i = 0; i < 2; i++) {
                            for (int j = 0; j < 3; j++) {
                                vo_vel_ned[i] += PX4_R(att.R, i, j) * vo_vel_pitched[j];
                            }
                        }

                        if (!isfinite(vo_vel_ned[0]) || !isfinite(vo_vel_ned[1]) || !isfinite(vo_vel_pitched[2]))
                        {
                            write_debug_log("BAD VO",0.123,vo_vel_pitched,vo_vel_ned,0,0,0,0,0);
                            use_vo = false;
                        }

                    }
                    else //GD_QUAD
                    {
                        vo_vel[1] = fovis_state.fovis_pose[0];// /(fovis_state.time_del_nsec/1e9);
                        vo_vel[0] = fovis_state.fovis_pose[2];// /(fovis_state.time_del_nsec/1e9);
                        vo_vel[2] = fovis_state.fovis_pose[1];
                        //Fovis state in optical frame is neagtive
                        //vo_vel[1] = fovis_state.fovis_pose[0];// /(fovis_state.time_del_nsec/1e9);
                        //vo_vel[0] = -fovis_state.fovis_pose[2];// /(fovis_state.time_del_nsec/1e9);
                        //vo_vel[2] = z_est[1];//TODO should be the val from VO

                        for (int i = 0; i < 2; i++) {
                            for (int j = 0; j < 3; j++) {
                                vo_vel_ned[i] += PX4_R(att.R, i, j) * vo_vel[j];
                            }
                        }
                    }


                    corr_vo[0]=vo_vel_ned[0] - x_est[1];
                    corr_vo[1]=vo_vel_ned[1] - y_est[1];
                    corr_vo[2]=vo_vel_ned[2] - z_est[1];
                    debug_vel[0] = vo_vel_ned[0];
                    debug_vel[1] = vo_vel_ned[1];

                    if(use_vo)
                    {
                    z(0) = vo_vel_ned[0];
                    z(1) = vo_vel_ned[1];
                    z(2) = vo_vel_ned[2];

                    Q(0,0) = Q(1,1) = params.q_vo_good;
                    Q(2,2) = params.q_vo_z_good;
                    }
                }
                else
                {
//                    z(0) = 0.0;
//                    z(1) = 0.0;
//                    z(2) = 0.0;
                    Q(0,0) = Q(1,1) = params.q_vo_bad;
                    Q(2,2) = params.q_vo_z_bad;
                }
                //</GD>


            }

            /* optical flow */
            orb_check(optical_flow_sub, &updated);

            if (updated /*&& (params.use_flow == 1)*/) {
                orb_copy(ORB_ID(optical_flow), optical_flow_sub, &flow);

                /* calculate time from previous update */
                float flow_dt = flow_prev > 0 ? (flow.timestamp - flow_prev) * 1e-6f : 0.1f;
                flow_prev = flow.timestamp;

//                if (flow.ground_distance_m > 0.31f && flow.ground_distance_m < 4.0f && att.R[2][2] > 0.7 && flow.ground_distance_m != sonar_prev) {
//                    sonar_time = t;
//                    sonar_prev = flow.ground_distance_m;
//                    corr_sonar = flow.ground_distance_m + surface_offset + z_est[0];
//                    corr_sonar_filtered += (corr_sonar - corr_sonar_filtered) * params.sonar_filt;

//                    if (fabsf(corr_sonar) > params.sonar_err) {
//                        /* correction is too large: spike or new ground level? */
//                        if (fabsf(corr_sonar - corr_sonar_filtered) > params.sonar_err) {
//                            /* spike detected, ignore */
//                            corr_sonar = 0.0f;
//                            sonar_valid = false;
//                            Q(5,5) = params.q_sonar_bad;
//                            mavlink_log_info(mavlink_fd, "SPIKE");

//                        } else {
//                            /* new ground level */
//                            surface_offset -= corr_sonar;
//                            surface_offset_rate = 0.0f;
//                            corr_sonar = 0.0f;
//                            corr_sonar_filtered = 0.0f;
//                            sonar_valid_time = t;
//                            sonar_valid = true;
//                            local_pos.surface_bottom_timestamp = t;
//                            mavlink_log_info(mavlink_fd, "[inav] new surface level: %.2f", surface_offset);
//                        }

//                    } else {
//                        /* correction is ok, use it */
//                        sonar_valid_time = t;
//                        sonar_valid = true;

//                        if(flow.ground_distance_m < 2.0)
//                            z(5) = -flow.ground_distance_m;
//                        Q(5,5) = params.q_sonar_good;//F_B;//0.001;
//                    }
//                }

                if (flow.ground_distance_m > 0.31f && flow.ground_distance_m < 4.0f && PX4_R(att.R, 2,2) > 0.7 && flow.ground_distance_m != sonar_prev)
                {
                    sonar_time = t;
                    sonar_prev = flow.ground_distance_m;
                    if(flow.ground_distance_m < 2.0)
                    {
                        float result;
                        bool valid = mov_avg(flow.ground_distance_m,&result);
                        if(valid){
                            sonar_valid_time = t;
                            sonar_valid = true;
                            z(5) = -result;
                            Q(5,5) = params.q_sonar_good;//F_B;//0.001;
                        }
                        else
                            Q(5,5) = params.q_sonar_bad;
//                        warnx("Result vs Meas %.2f %.2f",result,flow.ground_distance_m);
                    }
                }

                float flow_q = flow.quality / 255.0f;
                float dist_bottom = -z_est[0];//-z(5);//- z_est[0] - surface_offset;

                if (dist_bottom > 0.3f && flow_q > params.flow_q_min && (t < sonar_valid_time + sonar_valid_timeout) && PX4_R(att.R, 2, 2) > 0.7 && (params.use_flow == 1)) {
                    /* distance to surface */
                    float flow_dist = dist_bottom / PX4_R(att.R, 2, 2);
                    /* check if flow if too large for accurate measurements */
                    /* calculate estimated velocity in body frame */
                    float body_v_est[2] = { 0.0f, 0.0f };

                    for (int i = 0; i < 2; i++) {
                        body_v_est[i] = PX4_R(att.R, 0, i) * x_est[1] + PX4_R(att.R, 1, i) * y_est[1] + PX4_R(att.R, 2, i) * z_est[1];
                    }

                    /* set this flag if flow should be accurate according to current velocity and attitude rate estimate */
                    flow_accurate = fabsf(body_v_est[1] / flow_dist - att.rollspeed) < max_flow &&
                            fabsf(body_v_est[0] / flow_dist + att.pitchspeed) < max_flow;

                    /* convert raw flow to angular flow (rad/s) */
                    float flow_ang[2];
                    //todo check direction of x und y axis
                    flow_ang[0] = flow.pixel_flow_x_integral/(float)flow.integration_timespan*1000000.0f;//flow.flow_raw_x * params.flow_k / 1000.0f / flow_dt;
                    flow_ang[1] = flow.pixel_flow_y_integral/(float)flow.integration_timespan*1000000.0f;//flow.flow_raw_y * params.flow_k / 1000.0f / flow_dt;
                    /* flow measurements vector */
                    float flow_m[3];
                    flow_m[0] = flow_ang[1] * flow_dist;
                    flow_m[1] = -flow_ang[0] * flow_dist;
                    flow_m[2] = z_est[1];

                    if(1)//SB_QUAD
                    {
                        //CORRECTING FOR TRANSLATION
                        if (isfinite(att.pitchspeed))
                            flow_m[0] -= att.pitchspeed * params.sens_tf;//23cm translation
                        if (isfinite(att.rollspeed))
                            flow_m[1] += att.rollspeed * params.sens_tf;
                    }

                    /* velocity in NED */
                    float flow_v[2] = { 0.0f, 0.0f };

                    /* project measurements vector to NED basis, skip Z component */ //GD COMMENTED following rotation correc
                    if(1) //GD
                    {
                    for (int i = 0; i < 2; i++) {
                        for (int j = 0; j < 3; j++) {
                            flow_v[i] += PX4_R(att.R, i, j) * flow_m[j];
                        }
                    }
                    }
                    else
                    {
                        flow_v[0] = flow_m[0];
                        flow_v[1] = flow_m[1];
                    }


                    /* velocity correction */
                    corr_flow[0] = flow_v[0] - x_est[1];
                    corr_flow[1] = flow_v[1] - y_est[1];
                    debug_vel[2] = flow_v[0];
                    debug_vel[3] = flow_v[1];
                    /* adjust correction weight */
                    float flow_q_weight = (flow_q - params.flow_q_min) / (1.0f - params.flow_q_min);
                    w_flow = PX4_R(att.R, 2,2) * flow_q_weight / fmaxf(1.0f, flow_dist);


                    /* if flow is not accurate, reduce weight for it */
                    // TODO make this more fuzzy
                    if (!flow_accurate) {
                        w_flow *= 0.05f;
                    }

                    if (!isfinite(flow_v[0]) || !isfinite(flow_v[1]))
                    {
                        float debug[] = {flow_v[0], flow_v[1],0.0};
                        write_debug_log("BAD FLOW. Vals are just fillers",0.123,debug,y_est,0,0,0,0,0);
                        mavlink_log_info(mavlink_fd, "FLOW INVALID");
                        flow_valid = false;                        
//                        Q(3,3) = Q(4,4) = Q(5,5) = F_B;
                    }
                    else
                    {
                        flow_valid = true;

                        z(3) = flow_v[0];
                        z(4) = flow_v[1];

                        Q(3,3) = Q(4,4) = params.q_flow_good / flow_q;                        
                    }

                } else {
                    w_flow = 0.0f;
                    flow_valid = false;
//                    z(3) = 0.0;
//                    z(4) = 0.0;
//                    z(5) = 0.0;
                    Q(3,3) = Q(4,4) = params.q_flow_bad;                    
                }

                flow_updates++;
            }

        }
////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////uORB TOPIC POLLING DONE//////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
        /* check for timeout on FLOW topic */
        if ((flow_valid || sonar_valid) && t > flow.timestamp + flow_topic_timeout) {
            flow_valid = false;
            sonar_valid = false;
            warnx("FLOW timeout");
            mavlink_log_info(mavlink_fd, "[inav] FLOW timeout");
        }


        /* check for sonar measurement timeout */
        if (sonar_valid && t > sonar_time + sonar_timeout) {
            corr_sonar = 0.0f;
            sonar_valid = false;
        }

        float dt = t_prev > 0 ? (t - t_prev) / 1000000.0f : 0.0f;
        dt = fmaxf(fminf(0.02, dt), 0.002);		// constrain dt from 2 to 20 ms
        t_prev = t;

        /* use flow if it's valid and (accurate or no GPS available) */
        bool use_flow = flow_valid && flow_accurate;

        /* try to estimate position during some time after position sources lost */
        if (use_flow) {
            xy_src_time = t;
        }

        bool can_estimate_xy = false;
//        if(!use_vo)
//            can_estimate_xy = (t < xy_src_time + xy_src_timeout);
//        else
        can_estimate_xy = ((t < xy_src_time + xy_src_timeout) && (params.use_flow == 1)) || ((t - t_prev_vo) < (/*0.07*/2*1e6));//2 sec timeout
        global_pose_valid = (t < (t_prev_global_pose + global_src_timeout));

        bool dist_bottom_valid = (t < sonar_valid_time + sonar_valid_timeout) || (t < laser_valid_time + laser_valid_timeout);

        if (dist_bottom_valid) {
            /* surface distance prediction */
            surface_offset += surface_offset_rate * dt;

            /* surface distance correction */
            if (sonar_valid) {
                surface_offset_rate -= corr_sonar * 0.5f * params.w_z_sonar * params.w_z_sonar * dt;
                surface_offset -= corr_sonar * params.w_z_sonar * dt;
            }
        }

        float w_xy_gps_p = params.w_xy_gps_p * w_gps_xy;
        float w_xy_gps_v = params.w_xy_gps_v * w_gps_xy;
        float w_z_gps_p = params.w_z_gps_p * w_gps_z;

        /* reduce GPS weight if optical flow is good */
        if (use_flow && flow_accurate) {
            w_xy_gps_p *= params.w_gps_flow;
            w_xy_gps_v *= params.w_gps_flow;
        }


        /* accelerometer bias correction */
        float accel_bias_corr[3] = { 0.0f, 0.0f, 0.0f };


        if (use_flow) {
            accel_bias_corr[0] -= corr_flow[0] * params.w_xy_flow;
            accel_bias_corr[1] -= corr_flow[1] * params.w_xy_flow;
        }
        if(sonar_valid) {
            accel_bias_corr[2] -= (z(5) - z_est[0]) * params.w_z_sonar * params.w_z_sonar;
        }

        if(laser_good)
        {
            accel_bias_corr[2] -= (z(6) - z_est[0]) * 1.0 ;//params.w_z_sonar * params.w_z_sonar;
        }

//        if (use_vo) {
//            accel_bias_corr[0] -= corr_vo[0] * params.w_xy_vo;
//            accel_bias_corr[1] -= corr_vo[1] * params.w_xy_vo;
//            accel_bias_corr[2] -= corr_vo[2] * params.w_xy_vo;
//        }

//        accel_bias_corr[2] -= corr_baro * params.w_z_baro * params.w_z_baro;

        /* transform error vector from NED frame to body frame */
        for (int i = 0; i < 3; i++) {
            float c = 0.0f;

            for (int j = 0; j < 3; j++) {
                c += PX4_R(att.R, i, j) * accel_bias_corr[j];
            }

            if (isfinite(c)) {
                acc_bias[i] += c * params.w_acc_bias * dt;
            }
        }

        if(!use_kf)
        {
            /* inertial filter prediction for altitude */
            inertial_filter_predict(dt, z_est);

            /* inertial filter correction for altitude */
            inertial_filter_correct(corr_baro, dt, z_est, 0, params.w_z_baro);
            inertial_filter_correct(corr_gps[2][0], dt, z_est, 0, w_z_gps_p);
            inertial_filter_correct(corr_acc[2], dt, z_est, 2, params.w_z_acc);
            //        inertial_filter_correct(corr_vo[2],dt,z_est,1,params.w_xy_vo );
        }
        float x_est_prev[3], y_est_prev[3];

        memcpy(x_est_prev, x_est, sizeof(x_est));
        memcpy(y_est_prev, y_est, sizeof(y_est));

        if (can_estimate_xy || sonar_valid || laser_good/*|| 1*/) {

            if(!use_kf) //inertial filter
            {
                /* inertial filter prediction for position *///GD Commented following 2 lines
                inertial_filter_predict(dt, x_est);
                inertial_filter_predict(dt, y_est);

//                u(0) = x_est[0] ;
//                u(3) = x_est[1] ;
//                u(6) = x_est[2] ;

//                u(1) = y_est[0] ;
//                u(4) = y_est[1] ;
//                u(7) = y_est[2] ;
            }
            else //Kalman Filter
            {
                predict(dt);
//                if(fabs(u(3)) > 1.0) u(3) = 1.0*u(3)/fabs(u(3));
//                if(fabs(u(4)) > 1.0) u(4) = 1.0*u(4)/fabs(u(4));
//                if(fabs(u(5)) > 2.0) u(5) = 2.0*u(5)/fabs(u(5));
                x_est[0] = u(0);
                x_est[1] = u(3);
                x_est[2] = u(6);

                y_est[0] = u(1);
                y_est[1] = u(4);
                y_est[2] = u(7);

                z_est[0] = u(2);
                z_est[1] = u(5);
                z_est[2] = u(8);
//                warnx("Pred %.2f %.2f dt:%.2f",u(0),u(1),dt);
            }

            if (!isfinite(x_est[0]) || !isfinite(y_est[0])) {
                write_debug_log("BAD ESTIMATE AFTER PREDICTION", dt, x_est, y_est, z_est, corr_acc, corr_gps, w_xy_gps_p, w_xy_gps_v);
                memcpy(x_est, x_est_prev, sizeof(x_est));
                memcpy(y_est, y_est_prev, sizeof(y_est));
            }

            /* inertial filter correction for position */ //GD Commented following 2 lines
            if(!use_kf)//intertial filter
            {
                if((params.use_acc == 1) && (use_flow || use_vo))
                {
                    inertial_filter_correct(corr_acc[0], dt, x_est, 2, params.w_xy_acc);
                    inertial_filter_correct(corr_acc[1], dt, y_est, 2, params.w_xy_acc);
                }


                if(use_vo && (params.use_vo == 1))
                {
                    inertial_filter_correct(corr_vo[0], dt, x_est, 1, params.w_xy_vo );
                    inertial_filter_correct(corr_vo[1], dt, y_est, 1, params.w_xy_vo );
                }

                if (use_flow && (params.use_flow == 1)) {
                    inertial_filter_correct(corr_flow[0], dt, x_est, 1, params.w_xy_flow * w_flow * w_rel[0]);
                    inertial_filter_correct(corr_flow[1], dt, y_est, 1, params.w_xy_flow * w_flow * w_rel[0]);
                }

                if (use_global_pose && (params.use_g == 1)) {
                    inertial_filter_correct(corr_global_update[0], dt, x_est, 0, params.w_xy_g);
                    inertial_filter_correct(corr_global_update[1], dt, y_est, 0, params.w_xy_g);
                }

//                u(3) = x_est[1];
//                u(4) = y_est[1];
            }
            else //Kalman filter
            {
//                warnx("correct");
//                z(0) = debug_vel[0];
//                z(1) = debug_vel[1];
//                z(2) = 0;
//                z(3) = debug_vel[2];
//                z(4) = debug_vel[3];
//                z(5) = 0;
//                z.print();
//                z(3) = z(4) = z(5) = 1;
//                z(0) = z(1) = z(2) = 1;
                correct(dt);
                x_est[0] = u(0);
                x_est[1] = u(3);
                x_est[2] = u(6);

                y_est[0] = u(1);
                y_est[1] = u(4);
                y_est[2] = u(7);

//                printf("%.7f %.7f %.7f %.7f \n",z_est[0],z_est[1],u(2),u(5));
//                if(fabs(z_est[0]-u(2))< 0.5)
//                {
                    z_est[0] = u(2);
                    z_est[1] = u(5);
                    z_est[2] = u(8);
//                }
//                else
//                    mavlink_log_info(mavlink_fd, "[inav] Z jump");

                if(print_ctr == 100 )
                {
//                    warnx("U: \n");
//                    u.print();
//                    warnx("\nSIG: \n%.7f \t%.7f \t%.7f \n%.7f \t%.7f \t%.7f",sig(0,0),sig(1,1),sig(2,2),sig(3,3),sig(4,4),sig(5,5));
//                   mavlink_log_info(mavlink_fd,"Bias: %.7f %.7f %.7f",acc_bias[0],acc_bias[1],acc_bias[2]);

                    bool status[3] = {0,0,0};
//                    if(sig(0,0) < 0.0001) warnx("X\tGOOD");else warnx("X\tBAD");
//                    if(sig(1,1) < 0.0001) warnx("Y\tGOOD");else warnx("Y\tBAD");
//                    if(sig(2,2) < 0.0002) warnx("Z\tGOOD");else warnx("Z\tBAD");
                    if(sig(0,0) < 0.007) status[0] = 1;
                    if(sig(1,1) < 0.007) status[1] = 1;
                    if(sig(2,2) < 0.003) status[2] = 1;
//                    mavlink_log_info(mavlink_fd, "[kalman_pose] XYZ: %d %d %d",status[0],status[1],status[2]);
//                    sig.print();
//                    warnx("Q: \n");
//                    Q.print();
//                    warnx("R: \n");
//                    R.print();
                    print_ctr = 0;
//                    math::Vector<9> data;
//                    data(0)=x_est[1];
//                    data(1)=y_est[1];
//                    data(2)=u(3);
//                    data(3)=u(4);
//                    data(4)=debug_vel[0];//VO v
//                    data(5)=debug_vel[1];//VO v
//                    data(6)=debug_vel[2];//FO v
//                    data(7)=debug_vel[3];//FO v

//                    write_debug_log2("U:",dt,data);
                }
                else
                    print_ctr++;
            }
            float vel_lim = 1.5;
            if(fabs(x_est[1]) > vel_lim) x_est[1] = vel_lim * x_est[1]/fabs(x_est[1]);
            if(fabs(y_est[1]) > vel_lim) y_est[1] = vel_lim * y_est[1]/fabs(y_est[1]);
            if(fabs(z_est[1]) > vel_lim) z_est[1] = vel_lim * z_est[1]/fabs(z_est[1]);


            if (!isfinite(x_est[0]) || !isfinite(y_est[0])) {
                write_debug_log("BAD ESTIMATE AFTER CORRECTION", dt, x_est, y_est, z_est, corr_acc, corr_gps, w_xy_gps_p, w_xy_gps_v);
                memcpy(x_est, x_est_prev, sizeof(x_est));
                memcpy(y_est, y_est_prev, sizeof(y_est));
                memset(corr_acc, 0, sizeof(corr_acc));
                memset(corr_gps, 0, sizeof(corr_gps));
                memset(corr_flow, 0, sizeof(corr_flow));
            }
//            printf("%.7f %.7f %.7f %.7f ",debug_vel[0],debug_vel[1],debug_vel[2],debug_vel[3]);
//            printf("%.7f %.7f %d\n",x_est[1],y_est[1], fovis_state.estimate_status);
//            printf("%.7f %.7f %.7f %.7f \n",x_est[0],y_est[0],u(0),u(1));
        }
//        else //cant estimate xy, reset the velocities and  to zero.
        if(!can_estimate_xy)
        {
            z(0) = z(1) = z(2) = z(3) = z(4) = z(7) = z(8) = z(9) = 0.0;//VO_XYZ,OF_XY,ACC_XYZ
            u(3) =u(4) =u(6) =u(7) =0.0;
//            x_est[1] = 0.0f;
//            y_est[1] = 0.0f;
        }

        /* detect land */
        alt_avg += (- z_est[0] - alt_avg) * dt / params.land_t;
        float alt_disp2 = - z_est[0] - alt_avg;
        alt_disp2 = alt_disp2 * alt_disp2;
        float land_disp2 = params.land_disp * params.land_disp;
        /* get actual thrust output */
        float thrust = armed.armed ? actuator.control[3] : 0.0f;

        if (landed) {
            if (alt_disp2 > land_disp2 && thrust > params.land_thr) {
                landed = false;
                landed_time = 0;
            }

        } else {
            if (alt_disp2 < land_disp2 && thrust < params.land_thr) {
                if (landed_time == 0) {
                    landed_time = t;    // land detected first time

                } else {
                    if (t > landed_time + params.land_t * 1000000.0f) {
                        landed = true;
                        landed_time = 0;
                    }
                }

            } else {
                landed_time = 0;
            }
        }

        if (verbose_mode) {
            /* print updates rate */
            if (t > updates_counter_start + updates_counter_len) {
                float updates_dt = (t - updates_counter_start) * 0.000001f;
                warnx(
                    "updates rate: accelerometer = %.1f/s, baro = %.1f/s, gps = %.1f/s, attitude = %.1f/s, flow = %.1f/s",
                    accel_updates / updates_dt,
                    baro_updates / updates_dt,
                    gps_updates / updates_dt,
                    attitude_updates / updates_dt,
                    flow_updates / updates_dt);
                updates_counter_start = t;
                accel_updates = 0;
                baro_updates = 0;
                gps_updates = 0;
                attitude_updates = 0;
                flow_updates = 0;
            }
        }

        if (t > pub_last + pub_interval) {
            pub_last = t;
            /* publish local position */
            local_pos.xy_valid = can_estimate_xy;
            local_pos.v_xy_valid = can_estimate_xy;
            local_pos.xy_global = local_pos.xy_valid && false;
            local_pos.z_global = local_pos.z_valid && false;
            local_pos.x = x_est[0];
            local_pos.vx = x_est[1];
            local_pos.y = y_est[0];
            local_pos.vy = y_est[1];
//            local_pos.z = z_est[0]; //GD Commented
//            local_pos.vz = z_est[1]; //SJ commented
//            local_pos.landed = landed;
            local_pos.yaw = att.yaw;
            local_pos.dist_bottom_valid = dist_bottom_valid;

            if (local_pos.dist_bottom_valid) {
//              local_pos.dist_bottom = -z_est[0] - surface_offset;
//				local_pos.dist_bottom_rate = -z_est[1] - surface_offset_rate;
//              local_pos.z = -local_pos.dist_bottom;
                local_pos.dist_bottom =  surface_offset; //SJ
                local_pos.dist_bottom_rate =  surface_offset_rate; //SJ
//                local_pos.z =z_est[0] + surface_offset ; //SJ
//                local_pos.vz=z_est[1] + surface_offset_rate; //SJ

                //KALMAN
                local_pos.z = z_est[0];//u(2);//-laser_range;
                local_pos.vz= z_est[1];//u(5);
//                local_pos.vz=-laser_range;
            }

            local_pos.timestamp = t;

            orb_publish(ORB_ID(vehicle_local_position), vehicle_local_position_pub, &local_pos);
              /* publish sensor corrections in state*/
            state_debug.corr_baro = corr_baro;
            state_debug.corr_flow_x = debug_vel[2];
            state_debug.corr_flow_y = debug_vel[3];
            state_debug.corr_sonar = corr_sonar;
            //state_debug.acc_bias_x = acc_bias[0];
            //state_debug.acc_bias_y = acc_bias[1];
            //state_debug.acc_bias_z = acc_bias[2];

            state_debug.acc_bias_x = corr_vo[0];
            state_debug.acc_bias_y = corr_vo[1];
//            state_debug.acc_bias_z = acc_bias[2];

            state_debug.corr_acc_x=debug_vel[0];
            state_debug.corr_acc_y=debug_vel[1];
            state_debug.corr_acc_z=corr_acc[2];
            //state_debug.sonar_valid=sonar_valid;
            //state_debug.xy_src_timeout=t<xy_src_timeout+xy_src_time;
            state_debug.flow_valid=flow_valid;
            state_debug.timestamp = t;
            orb_publish(ORB_ID(state_estimator_debug), state_estimator_debug_pub, &state_debug);
            /* publish global position */
//            global_pos.global_valid = local_pos.xy_global;

            if (local_pos.xy_global) {
                double est_lat, est_lon;
                map_projection_reproject(&ref, local_pos.x, local_pos.y, &est_lat, &est_lon);
                global_pos.lat = est_lat;
                global_pos.lon = est_lon;
//                global_pos.time_gps_usec = gps.time_gps_usec;
            }

            /* set valid values even if position is not valid */
            if (local_pos.v_xy_valid) {
                global_pos.vel_n = local_pos.vx;
                global_pos.vel_e = local_pos.vy;
            }

            if (local_pos.z_global) {
                global_pos.alt = local_pos.ref_alt - local_pos.z;
            }

            if (local_pos.z_valid) {
//                global_pos.baro_alt = - local_pos.z;
            }

            if (local_pos.v_z_valid) {
                global_pos.vel_d = local_pos.vz;
            }

            global_pos.yaw = local_pos.yaw;

            global_pos.timestamp = t;

            orb_publish(ORB_ID(vehicle_global_position), vehicle_global_position_pub, &global_pos);
        }
    }
}
