/* Copyright 2014 Sanjiban Choudhury
 * ca_trajectory_control.h
 *
 *  Created on: Apr 4, 2014
 *      Author: sanjiban
 */

#ifndef CA_TRAJECTORY_CONTROL_H_
#define CA_TRAJECTORY_CONTROL_H_

#include <nuttx/config.h>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <deque>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <math.h>
#include <poll.h>
#include <drivers/drv_hrt.h>
#include <arch/board/board.h>
#include <uORB/uORB.h>
#include <uORB/topics/vehicle_attitude_setpoint.h>
#include <uORB/topics/manual_control_setpoint.h>
#include <uORB/topics/actuator_controls.h>
#include <uORB/topics/vehicle_rates_setpoint.h>
#include <uORB/topics/vehicle_attitude.h>
#include <uORB/topics/vehicle_control_mode.h>
#include <uORB/topics/actuator_armed.h>
#include <uORB/topics/parameter_update.h>
#include <uORB/topics/vehicle_local_position.h>
#include <uORB/topics/position_setpoint_triplet.h>
#include <uORB/topics/vehicle_global_velocity_setpoint.h>
#include <uORB/topics/vehicle_local_position_setpoint.h>
#include <uORB/topics/ca_trajectory_msg.h>
#include <uORB/topics/vehicle_command.h>
#include <uORB/topics/ca_replan.h>
#include <uORB/topics/trajectory_control_debug.h>
#include <uORB/topics/ca_global_state.h>
#include <systemlib/param/param.h>
#include <systemlib/err.h>
#include <systemlib/systemlib.h>
#include <mathlib/mathlib.h>
#include <lib/geo/geo.h>
#include <mavlink/mavlink_log.h>
/**
 * Outer loop control based on trajectory or manual set points
 */
class TrajectoryControl {
 public:
  /**
   * Constructor
   */
  TrajectoryControl();

  /**
   * Destructor, also kills task.
   */
  ~TrajectoryControl();

  /**
   * Start task.
   *
   * @return    OK on success.
   */
  int   Start();

 private:

  bool  task_should_exit_;    /**< if true, task should exit */
  int   control_task_;      /**< task handle for task */
  int   mavlink_fd_;      /**< mavlink fd */

  int   att_sub_;       /**< vehicle attitude subscription */
  int   att_sp_sub_;      /**< vehicle attitude setpoint */
  int   control_mode_sub_;    /**< vehicle control mode subscription */
  int   params_sub_;      /**< notification of parameter updates */
  int   manual_sub_;      /**< notification of manual control updates */
  int   arming_sub_;      /**< arming status of outputs */
  int   local_pos_sub_;     /**< vehicle local position */
  int   pos_sp_triplet_sub_;    /**< position setpoint triplet */
  int 	ca_traj_sub_;   /**< ca trajectory message subscriber */
  int	ca_command_sub_;	/**< command<takeoff/land> message subscriber */
  int   ca_global_state_sub_;

  orb_advert_t  att_sp_pub_;      /**< attitude setpoint publication */
  orb_advert_t  local_pos_sp_pub_;    /**< vehicle local position setpoint publication */
  orb_advert_t  global_vel_sp_pub_;   /**< vehicle global velocity setpoint publication */
  orb_advert_t  replan_pub_;    /**< replan command publication */
  orb_advert_t  tc_debug_pub_;  /**< Logging variables for debug>*/

  struct vehicle_attitude_s     att_;     /**< vehicle attitude */
  struct vehicle_attitude_setpoint_s  att_sp_;    /**< vehicle attitude setpoint */
  struct manual_control_setpoint_s  manual_;    /**< r/c channel data */
  struct vehicle_control_mode_s   control_mode_;  /**< vehicle control mode */
  struct actuator_armed_s       arming_;    /**< actuator arming status */
  struct vehicle_local_position_s   local_pos_;   /**< vehicle local position */
  struct position_setpoint_triplet_s    pos_sp_triplet_;  /**< vehicle global position setpoint triplet */
  struct vehicle_local_position_setpoint_s  local_pos_sp_;    /**< vehicle local position setpoint */
  struct vehicle_global_velocity_setpoint_s global_vel_sp_; /**< vehicle global velocity setpoint */
  struct ca_traj_struct_s ca_traj_; /**< trajectory struct used for quad motion */
  struct trajectory_control_debug_s tc_debug_; /** <debug msgs for trajectory control*/
  struct ca_global_state_struct_s global_state;

  std::vector<ca_traj_struct_s> ca_traj_vec_;
  std::deque<ca_traj_struct_s> ca_traj_vec;
  //std::vector<math::Matrix<3,3> > ca_rotation_vec_;
  //std::vector<math::Vector<3 > > ca_translation_vec_;

  ca_traj_struct_s curr_seg;
  ca_replan_struct_s replan_;
  struct vehicle_command_s ca_command_; /**< command_long as in mavlink, struct used for quad takeoff/land*/
  unsigned int curr_seg_id;
  uint8_t replan_now;
  uint16_t replan_traj_id;
  hrt_abstime replan_time_usec;


  bool isLanding;
  bool isLaunching;

  struct {
    param_t thr_min;
    param_t thr_max;
    param_t z_p;
    param_t z_vel_p;
    param_t z_vel_i;
    param_t z_vel_d;
    param_t z_vel_max;
    param_t z_ff;
    param_t xy_p;
    param_t xy_vel_p;
    param_t xy_vel_i;
    param_t xy_vel_d;
    param_t xy_vel_max;
    param_t xy_ff;
    param_t tilt_max;
    param_t land_speed;
    param_t take_off_speed;
    param_t land_tilt_max;
    param_t land_alt;
    param_t take_off_alt;

    param_t rc_scale_pitch;
    param_t rc_scale_roll;
    param_t debug;
    param_t closed_loop_pos;
    param_t sync_mode;
    param_t thrustff_flag;
    param_t replan_period_sec;
    param_t replan_ahead_sec;

    param_t use_preset_z_sp;
  }   params_handles_;    /**< handles for interesting parameters */

  struct {
    float thr_min;
    float thr_max;
    float tilt_max;
    float land_speed;
    float take_off_speed;
    float land_tilt_max;
    float land_alt;
    float take_off_alt;

    float rc_scale_pitch;
    float rc_scale_roll;
    uint8_t debug;
    uint8_t closed_loop_pos;
    uint8_t sync_mode;
    uint8_t thrustff_flag;

    math::Vector<3> pos_p;
    math::Vector<3> vel_p;
    math::Vector<3> vel_i;
    math::Vector<3> vel_d;
    math::Vector<3> vel_ff;
    math::Vector<3> vel_max;
    math::Vector<3> sp_offs_max;

    float replan_period_sec;
    float replan_ahead_sec;
    uint8_t use_preset_z_sp;
  }   params_; /**< stores important parameters*/


  struct polynomial {
    math::Vector<7> x;  /**< X */
    math::Vector<7> y;  /**< Y */
    math::Vector<7> z;  /**< Z */
    math::Vector<7> psi;/**< heading*/
    float t_start;    /**< start time of segment*/
    float t_end;      /**< end time of segment*/

    void reset()
    {
        x.zero();
        y.zero();
        z.zero();
        psi.zero();
        t_start = 0;
        t_end = 0;
    }
  }; /**< represents a trajectory */

//  struct polynomial trajectory_; /**< trajectory polynomial */

  struct map_projection_reference_s ref_pos_;
  float ref_alt_;
  hrt_abstime ref_timestamp_;

  bool allow_spatial_position_control_;
  bool allow_altitude_position_control_;
  bool allow_spatial_velocity_control_;
  bool allow_altitude_velocity_control_;
  bool allow_spatial_thrust_control_;
  bool allow_altitude_thrust_control_;
  bool allow_thrust_feedforward_;

  bool reset_pos_sp_;
  bool reset_alt_sp_;
  bool reset_int_z_;
  bool reset_int_z_manual_;
  bool reset_int_xy_;
  bool reset_ref_traj_time_;
  bool new_traj_;
  int curr_traj_seg_;
  bool async_mode;

  math::Vector<3> pos_;
  math::Vector<3> pos_ref_;
  math::Vector<3> no_seg_pos_ref_;  /** position reference for auto mode when no segment is found**/
  math::Vector<3> pos_sp_;
  math::Vector<3> vel_;
  math::Vector<3> sp_move_rate_;
  math::Vector<3> vel_sp_;
  math::Vector<3> vel_prev_;      /**< velocity on previous step */
  math::Vector<3> thrust_traj_;
  math::Vector<3> thrust_int_;
  double psi_traj_;
  double no_seg_yaw_ref_;
  hrt_abstime ref_traj_time_;

  double dt_;

  int control_counter; //TODO add by shichao
  float yaw_ref;
  float rot_ref[3][3];
  hrt_abstime start_time;
  bool whether_replan;
  hrt_abstime prev_replan_time;
//  float replan_period_sec;
//  float replan_ahead_sec;


  /**
   * Shim for calling TaskMain from task_create.
   */
  static void TaskMainTrampoline(int argc, char *argv[]);

  /**
   * Main sensor collection task.
   */
  void    TaskMain() __attribute__((noreturn));


  void ComputeControlInput();

  // Position feedback
  bool PositionFeedback(const math::Vector<3> &pose_sp, const math::Vector<3> &sp_move_rate, math::Vector<3> &vel_sp);

  // Velocity feedback
  bool VelocityFeedback(const math::Vector<3> &vel_sp, const math::Vector<3> &sp_move_rate, math::Vector<3> &control);

  // Feedforward

  // Utilities

  void SaturateVector(const math::Vector<3> &vec_max, math::Vector<3> &vec);

  bool PublishLocalSetPoint();

  bool PublishGlobalVelSetPoint();

  void PublishReplan();

  // Meta actions
  void GetManualSetPoint();

  bool AttainIdleState();

  void ResetAltitudeIntegrator();

  void ResetSpatialIntegrator();

  void SaturateThrust(math::Vector<3> &thrust_sp);

  void ComputeDesiredRotation(math::Vector<3> &thrust_sp, math::Matrix<3, 3> &R);

  // Conditions
  /**
   * @return TRUE if idle
   */
  bool IsIdle();

  /**
   * @return TRUE if control should be allowed
   */
  bool AllowControl();

  /**
   * @return TRUE if manual input is allowed
   */
  bool AllowManualInput();

  bool AllowAltitudePositionControl();

  bool AllowSpatialPositionControl();

  bool AllowSpatialVelocityControl();

  bool AllowAltitudeVelocityControl();

  bool AllowThrustComputing();

  bool AllowAltitudeThurstComputing();

  bool AllowSpatialThurstComputing();

  bool AllowFeedForwardThrust();

  bool IsLanding();

  bool LimitSpeed();

  /**
   * Update our local parameter cache.
   */
  int     ParametersUpdate(bool force);

  /**
   * Update control outputs
   */
  void    ControlUpdate();

  /**
   * Check for changes in subscribed topics.
   */
  void    PollSubscriptions();

  /**
   * Scales control
   * @param ctl
   * @param end
   * @param dz
   * @return
   */
  static float  ScaleControl(float ctl, float end, float dz);

  /**
   * Update reference for local position projection
   */
  void    UpdateRef();
  /**
   * Reset position setpoint to current position
   */
  void    ResetPosSP();

  /**
   * Reset altitude setpoint to current altitude
   */
  void    ResetAltSp();
  /**
   * Reset reference point for auto in case no segment is found. This serves as position setpoint
   */
  void    ResetNoSegPosRef();

  void    ResetRefTrajTime(hrt_abstime t);

  void    SetRefTrajTime(uint64_t t);

  /**
   * Select between barometric and global (AMSL) altitudes
   */
  void    SelectAlt(bool global);

  /**
   * Trajectory thrust extractor call back
   */
  void UpdateTransformForTrajectory();
  void RotateTrajectory();
  void   UpdateFromTrajectory(hrt_abstime curr_time);

  /**
   * Trajectory thrust generator
   */
//   void GenerateThrust(float t, bool segment_found);
  void GenerateThrust(float t, bool segment_found);

  /**
   * Trajectory continuity validator
   */
  bool checkTraj(ca_traj_struct_s new_traj);
};

namespace traj_control
{

/* oddly, ERROR is not defined for c++ */
#ifdef ERROR
# undef ERROR
#endif
static const int ERROR = -1;

extern TrajectoryControl *g_control;
}



#endif /* CA_TRAJECTORY_CONTROL_H_ */
